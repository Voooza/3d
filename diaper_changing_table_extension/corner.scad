
// configuration

length = 70;
height = 55; // measured
depth = 20;

wall_depth = 15;

wood_height = 10; // measured
wood_length = length - 34; // measured / counted
wood_screw_pos = [50, height - wood_height-10, depth/2];

slope_length = 70;
slope_heigth = wall_depth;
slope_angle = 60;
slope_prolong = 30;

slope_support_length = 20;
slope_support_height = 40;


slope_screw_offset_ratio = tan(slope_angle);

slope_screw_1_offset = 10;
slope_screw_2_offset = 45;

// configuration end

// calculated constants

offset_x = sin(slope_angle)*slope_prolong;
offset_y = cos(slope_angle)*slope_prolong;


hole_1_length = length - wood_length - (2 * wall_depth);
hole_1_height = height - 2 * wall_depth;

hole_2_length = length - (2 * wall_depth);
hole_2_height = height - wood_height - 2 * wall_depth;

hole_2_x_offset = wall_depth;
hole_2_y_offset = wall_depth;


// calculated constants end

module screw(){
    head_diameter = 8.1;
    head_height = 4;
    body_diameter = 4;
    body_length = 100;
    screwdriver_length = 100;
    cylinder(h = head_height, d1 = head_diameter, d2 = body_diameter, $fn = 50);
    translate([0,0,head_height])
    cylinder(h = body_length, d1 = body_diameter, d2 = body_diameter, $fn = 50);
    translate([0,0,-screwdriver_length])
    cylinder(h = screwdriver_length, d1 = head_diameter, d2 = head_diameter, $fn = 50);
}

// actual object
module body(){
    linear_extrude(height = depth,  convexity = 10, twist = 0)
    difference(){
        union(){
            polygon(points=[
                [0,0],
                [length,0],
                [length, height - wood_height],
                [length - wood_length ,height - wood_height],
                [length - wood_length ,height],
                [0 ,height]
                ]);
            translate([0, height])
            translate([offset_y, -offset_x])
            mirror([1,0,0])
            rotate(slope_angle, [0,0,1])  
            square(size = [slope_length + slope_prolong, slope_heigth]);
            translate([-slope_support_length, height])
            // slope support
            polygon(points=[
                [0,0],
                [slope_support_length,0],
                [slope_support_length - 10, slope_support_height],
                [0 ,slope_support_height]
                ]);
        };
        union(){
            // hole 1 - removed as it is probably not needed
            // translate([wall_depth, wall_depth])
            // square(size = [hole_1_length, hole_1_height]);
            
            // hole 2
            translate([hole_2_x_offset, hole_2_y_offset])
            square(size = [hole_2_length, hole_2_height]);
        };
    };
};


module full(){
    difference(){
        body();
        union(){
            translate(wood_screw_pos)
            rotate([0,90,90])
            screw();
            
            translate([0 - (slope_screw_1_offset/slope_screw_offset_ratio),height + slope_screw_1_offset,depth/2])
            rotate([slope_angle,90,90])
            screw();
            translate([0 - (slope_screw_2_offset/slope_screw_offset_ratio),height + slope_screw_2_offset,depth/2])
            rotate([slope_angle,90,90])
            screw();
        }
    }
}

full();

translate([length + 10, height + 70,0])
rotate([0,0,180])
full();

    