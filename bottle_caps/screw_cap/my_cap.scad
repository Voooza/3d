$fn=30;

translate([0,0,3])

for ( i = [0 : 4 : 1080] ){
    rotate( [0, 0, i])
    translate([0, 0, i/100])
    c();
}




difference(){
    union(){
        cylinder(h=20, r=15);
        grip();
    }
    translate([0,0,1])
    cylinder(h=20, r=14);
}

difference(){
    cylinder(h=5, d=13);
    translate([0,0,1])
    cylinder(h=5, d=11);
}


module c(){
    translate([13.2, 0, 0])
    rotate([0,45,0])
    cube();
}

module grip(){
    for ( i = [0 : 1 : 6] ){
        rotate( [0, 0, i*60])
        translate([11, 0, 0])
        cylinder(h=20, r=5, center=false);
    };
}
