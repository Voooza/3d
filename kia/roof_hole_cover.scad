width = 53;
height = 18.5;
depth = 2;
corner_radius = 2;

handle_radius = 20;
handle_height = 2.2;
handle_depth = 1.1;

lock_width = 37.5;
lock_height = 11;
lock_depth = 3;

lock_tooth_width = 4;
lock_tooth_height = 2;
lock_tooth_depth = 2;

big_tooth_width = 15;
big_tooth_height = 11;
big_tooth_depth = 2;




body();
lock();
translate([-big_tooth_width/2 + 15,-big_tooth_height/2,lock_depth])
cube([big_tooth_width,big_tooth_height,big_tooth_depth]);


module lock(){
    translate([-lock_width/2-1,-lock_height/2,0])
    cube([lock_width,lock_height,lock_depth]);
    translate([-lock_width/2-1,lock_height/2,1])
    cube([lock_tooth_width,lock_tooth_height,lock_tooth_depth]);
    translate([-lock_width/2-1,-lock_height/2-lock_tooth_height,1])
    cube([lock_tooth_width,lock_tooth_height,lock_tooth_depth]);
    translate([-lock_width/2-1+13,lock_height/2,1])
    cube([lock_tooth_width,lock_tooth_height,lock_tooth_depth]);
    translate([lock_width/2-1-4-lock_tooth_width,lock_height/2,1])
    cube([lock_tooth_width,lock_tooth_height,lock_tooth_depth]);
    translate([lock_width/2-1-4-lock_tooth_width,-lock_height/2-lock_tooth_height,1])
    cube([lock_tooth_width,lock_tooth_height,lock_tooth_depth]);
}



module body(){
    difference(){
        rotate([0,180,0])
        linear_extrude(height = depth, center = false, convexity = 10, scale=[0.9,0.8])
        hull(){
            translate([width/2 - corner_radius,height/2 - corner_radius])
            circle(r=corner_radius, $fn=20);
            mirror([0,1])
            translate([width/2 - corner_radius,height/2 - corner_radius])
            circle(r=corner_radius, $fn=20);
            mirror([1,0])
            translate([width/2 - corner_radius,height/2 - corner_radius])
            circle(r=corner_radius, $fn=20);
            mirror([1,0])
            mirror([0,1])
            translate([width/2 - corner_radius,height/2 - corner_radius])
            circle(r=corner_radius, $fn=20);
        };
        translate([15,0,-(handle_radius + handle_depth)])
        rotate([0,90,0])
        cylinder(h = handle_height, r1 = handle_radius, r2 = handle_radius, center = false, $fn=80);
    };
}

