stand_length = 150;
height = 10;

material_strength = 20;

support_legth = 60;

lenovo_height = 25;
dell_height = 24;

angle = 60;

lenovo_adjusted_height = lenovo_height/sin(angle);
dell_adjusted_height = dell_height/sin(angle);
adjusted_height = height/sin(angle);

linear_extrude(height = material_strength, center = true, convexity = 10, twist = 0)
union(){
    square([stand_length, height]);
    translate([0, height/2])
    rotate([0,0,angle])
    hull(){
        circle(d = height);
        translate([support_legth - height, 0])
        circle(d = height);
    }
    translate([dell_height + adjusted_height, height/2])
    rotate([0,0,angle])
    hull(){
        circle(d = height);
        translate([support_legth - height, 0])
        circle(d = height);
    }
    translate([dell_height + adjusted_height + lenovo_height + adjusted_height, height/2])
    rotate([0,0,angle])
    hull(){
        circle(d = height);
        translate([(support_legth - height) * 1.4, 0])
        circle(d = height);
    }
    translate([stand_length- height/2, height/2])
    rotate([0,0,angle * 2])
    hull(){
        circle(d = height);
        translate([(support_legth - height) * 1.4, 0])
        circle(d = height);
    }
}