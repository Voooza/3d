depth = 20;
corpus_edge_radius = 15;
korpus_length = 120;
railing_radius = 13.5;

screw_diameter = 6;
screw_diameter_tolerance = 0.1;
head_diameter = 12;



module screw(length = 40, head_face_count = 6, avoid_bridge = 0){
    if(avoid_bridge == 1){
        translate([-5.2,-40,-1])
        cube([10.4,40,6]);
    }
    rotate([0,0,30])
    union(){
        cylinder(h = length, d1 = screw_diameter + screw_diameter_tolerance, d2 = screw_diameter + screw_diameter_tolerance, $fn=20);
        translate([0,0,-1])
        cylinder(h = 6, d1 = head_diameter, d2 = head_diameter, $fn = head_face_count);
    }
}

// main corpus body
module corpus_body(){
    hull(){
        translate([-corpus_edge_radius,-corpus_edge_radius,0])
        cube([2 * corpus_edge_radius,2 * corpus_edge_radius,depth]);
        translate([korpus_length/2 - 3 * corpus_edge_radius,-corpus_edge_radius,0])
        cube([2 * corpus_edge_radius,2 * corpus_edge_radius,depth]);
    };
};

module railing(){
    translate([0,0,-1])
    cylinder(h = depth + 2, r1 = railing_radius, r2 = railing_radius, $fn=100);
}


difference(){
    corpus_body();
    union(){
        // cut the hole for railing
        translate([corpus_edge_radius,corpus_edge_radius,0])
        railing();
        //cut the hole for railing binding screws
        translate([-corpus_edge_radius/2,-corpus_edge_radius,depth/2])
        rotate([-90,0,0])
        screw(avoid_bridge = 1,head_face_count = 40);
        translate([corpus_edge_radius/2 + corpus_edge_radius*2,-corpus_edge_radius,depth/2])
        rotate([-90,0,0])
        screw(avoid_bridge = 1,head_face_count = 40);
        //cut the hole for join screw
        translate([korpus_length - 2 * corpus_edge_radius,0,0])
//        rotate([-90,0,0])
        screw();
    };
}
