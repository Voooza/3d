module vyrez(){
    translate([-3,0,0])
    hull(){
        cylinder(h = 3, r1 = 3, r2 = 4);
        translate([6,0,0])
        cylinder(h = 3, r1 = 3, r2 = 4);
    };
};

difference(){
    difference(){
        difference(){
            cylinder(h = 3, d1 = 84.5, d2 = 84.5, $fn=200);
            cylinder(h = 20, d1 = 61, d2 = 61, $fn=200,center=true);
        };
        translate([42,0,2.1])
        vyrez();
    };
    translate([-42,0,2.1])
    vyrez();
};
    
