width = 150;
height = 100;
depth = 3;
corner_radius = 10;

socket_depth = 20;
socket_wall_depth = 2;
cuboid_socket_x_offset = 5;
cuboid_socket_y_offset = 3;

rotator_radius = 10;// todo measure

cuboid_width = 20;// todo measure
cuboid_height = 10;// todo measure

holder();


module holder(){
    hull(){z();x();}
    hull(){z();y();}
    hull(){x();y();}
    translate([width/2, height - rotator_radius,0])
    rotator_socket();
    translate([cuboid_socket_x_offset, cuboid_socket_y_offset, 0])
    cuboid_socket();
    translate([width - cuboid_socket_x_offset - cuboid_width - 2 * socket_wall_depth, cuboid_socket_y_offset, 0])
    cuboid_socket();
}

module rotator_socket(){
//        translate([rotator_radius + socket_wall_depth,rotator_radius + socket_wall_depth,0])
    difference(){
        cylinder(h = socket_depth, r = rotator_radius + socket_wall_depth);
        
        union(){
            cylinder(h = socket_depth, r = rotator_radius);
            translate([0, 1-rotator_radius , socket_depth/2])
            rotate([90,0,0])
            cylinder(h = socket_wall_depth + 2, r = 1, $fn=6);
        }
    }
}

module cuboid_socket(){
    difference(){
        cube([cuboid_width + 2*socket_wall_depth, cuboid_height + 2*socket_wall_depth, socket_depth]);
        union(){
            translate([socket_wall_depth,socket_wall_depth,0])
            cube([cuboid_width, cuboid_height, socket_depth]);
            translate([cuboid_width/2 + socket_wall_depth, socket_wall_depth + 1, socket_depth/2])
            rotate([90,0,0])
            cylinder(h = socket_wall_depth + 2, r = 1, $fn=6);
        }
    }
}


module z(){
    translate([corner_radius,corner_radius,0])
    cylinder(h = depth, r1 = corner_radius, r2 = corner_radius, center = false, $fn = 40);
}

module x(){
    translate([width - corner_radius,corner_radius,0])
    cylinder(h = depth, r1 = corner_radius, r2 = corner_radius, center = false, $fn = 40);
}

module y(){
    translate([width/2,height - corner_radius,0])
    cylinder(h = depth, r1 = corner_radius, r2 = corner_radius, center = false, $fn = 40);
}



