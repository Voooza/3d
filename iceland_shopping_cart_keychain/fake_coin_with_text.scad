difference(){
    union(){
        cylinder(h=3, d1=25, d2=25, $fn = 200);
        difference(){
        hull(){
            translate([-5,0,0])
            cube([10,10,3]);
            translate([0,20,0])
            cylinder(h=3, d1=8, d2=8, $fn = 200);
        };
        translate([0, 21.5,-1])
        cylinder(h=5, d1=3, d2=3, $fn = 20);
        }
    };

    translate([2.5,-10,2])
    scale(0.5)
    rotate([0,0,90])
    linear_extrude(height = 3, center = false, convexity = 10, twist = 0)
    text("ICELAND");
}
