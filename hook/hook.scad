module hook(){
    linear_extrude(height = 5, convexity = 10, twist = 0)
    union(){
        square(size = [3, 15]);
        translate([3,12])
            square(size = [5, 3]);
        square(size = [50, 3]);
        translate([47,-36])
            square(size = [3, 36]);
        translate([37,-36])
            square(size = [10, 3]);
    };
};

module hookWithText(text){
    hook();
    translate([48,0,0.5])
    rotate([90,0,180])
    linear_extrude(height = 3.5, convexity = 10, twist = 0)
    text(text, size=4);
}

hookWithText("KOSS");

translate([15, 15, 0])
hookWithText("JABRA");