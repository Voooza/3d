cube([130,16,2]);
cube([130,2,5]);
translate([0,14,0])
cube([130,2,5]);
cube([2,14,5]);
translate([128,0,0])
cube([2,14,5]);

union(){
cube([10,2,20]);
translate([120,0,0])
cube([10,2,20]);
};


translate([0,14,0])
union(){
cube([10,2,20]);
translate([120,0,0])
cube([10,2,20]);
};

translate([0,2,0])
rotate([90,0,0])
difference(){
hull(){
    cube([130,20,2]);
    translate([65,100,0])
    cylinder(h = 2, d1 = 20, d2 = 20);
};
translate([10,10,-1])
hull(){
    cube([110,10,5]);
    translate([55,80,0])
    cylinder(h = 5, d1 = 10, d2 = 10);
};
};