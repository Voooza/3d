module earpiece_storage(elevation = 0, hole_depth = 20){
    difference(){
        hull(){
            translate([-2,0,0])
            cylinder(h = 20, d1 = 24, d2 = 24, $fn=100);
            translate([2,0,0])
            cylinder(h = 20, d1 = 24, d2 = 24, $fn=100);
        };
        hull(){
            translate([-2,0,elevation])
            cylinder(h = hole_depth, d1 = 20, d2 = 20, $fn=100);
            translate([2,0,elevation])
            cylinder(h = hole_depth, d1 = 20, d2 = 20, $fn=100);
        };
    };
};

module hole(){
    cylinder(h = 15, d1 = 3.6, d2 = 3.6, $fn=20);
}

difference(){
    union(){
        translate([-30,-5,0])
        cube([60,10,15]);
        translate([-30,-15,0])
        earpiece_storage(elevation = 2, hole_depth = 18);
        translate([-30,15,0])
        earpiece_storage(elevation = 2, hole_depth = 18);
        translate([30,-15,0])
        earpiece_storage();
        translate([30,15,0])
        earpiece_storage();
    };
    union(){
        hole();
        translate([8,0,0])hole();
        translate([-8,0,0])hole();
        translate([16,0,0])hole();
        translate([-16,0,0])hole();
        translate([24,0,0])hole();
        translate([-24,0,0])hole();
    };
};
